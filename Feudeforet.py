import sys
import getopt
import datetime
import argparse
from tkinter import *
from random import *



#initialisation d'un tableau d'identifiants
def init_tableau() :
    tableau = []
    canvas.delete("all")
    for ligne in range (0, rows+2) :
        tmp = []
        for colonne in range (0, cols+2) :
            x = colonne*cell_size
            y = ligne*cell_size
            if (ligne == 0) or (ligne == rows+1) or (colonne == 0) or (colonne == cols+1) :
                tmp.append(canvas.create_rectangle(x,y,x+cell_size,y+cell_size, fill = "black"))
            #cellules aléatoires en fonction du taux de boisement
            else :
                if randint(1,100) <=  afforestation*100 :
                    tmp.append(canvas.create_rectangle(x,y,x+cell_size,y+cell_size, fill = "green"))
                else :
                    tmp.append(canvas.create_rectangle(x,y,x+cell_size,y+cell_size, fill = "white"))
        tableau.append(tmp)
    return tableau

def update_tableau(set_fire, set_ashes, empty) :
    for i in range (0, len(set_fire)) :
        canvas.itemconfig(set_fire[i], fill = "red")
    for i in range (0, len(set_ashes)) :
        canvas.itemconfig(set_ashes[i], fill = "grey")
    for i in range (0, len(empty)) :
        canvas.itemconfig(empty[i], fill = "white")



def simulation() :
    is_burned = True
    set_fire = []
    set_ashes = []
    empty = []

    for i in range (1, rows+1) :
        for j in range (1, cols+1) :
            if (canvas.itemcget(foret[i][j], "fill") == "green") :
                if (canvas.itemcget(foret[i][j+1], "fill") == "red") or (canvas.itemcget(foret[i+1][j], "fill") == "red") or (canvas.itemcget(foret[i-1][j], "fill") == "red") or (canvas.itemcget(foret[i][j-1], "fill") == "red") :
                    if v.get() == "2" :
                        flammable = 0
                        if (canvas.itemcget(foret[i][j+1], "fill") == "red") :
                            flammable = flammable + 1
                        if (canvas.itemcget(foret[i+1][j], "fill") == "red") :
                            flammable = flammable + 1
                        if (canvas.itemcget(foret[i-1][j], "fill") == "red") :
                            flammable = flammable + 1
                        if (canvas.itemcget(foret[i][j-1], "fill") == "red") :
                            flammable = flammable + 1
                        if random() <= 1-1/(flammable+1) :
                            set_fire.append(foret[i][j])
                    else :
                        set_fire.append(foret[i][j])
            elif (canvas.itemcget(foret[i][j], "fill") == "grey") :
                is_burned = False
                empty.append(foret[i][j])
            elif (canvas.itemcget(foret[i][j], "fill") == "red") :
                is_burned = False
                set_ashes.append(foret[i][j])
            element_id = foret[i][j]
    update_tableau(set_fire, set_ashes, empty)
    if is_burned == False :
        master.after(int(1000*animation), simulation)

def click_callback(event) :
    x1 = event.x
    y1 = event.y
    element = canvas.find_closest(event.x, event.y)
    if canvas.itemcget(element, "fill") == "green" :
        canvas.itemconfig(element, fill = "red")

def main() :
    Start = Button(master, text = "Simuler", command = simulation, background = "lightgrey")
    Start.pack()
    
    #La fonction recrée en éfait une carte mais il est impossible de relancer une simulation ensuite pour une raison que j'ignore...
    #Reset = Button(master, text = "Recommencer", command = init_tableau,  background = "lightgrey")
    #Reset.pack()

    canvas.bind('<Button-1>', click_callback)

    master.mainloop()

rows = 15
cols = 15
cell_size = 30
animation = 0.5
afforestation = 0.5

parser = argparse.ArgumentParser()

parser.add_argument("-rows", help="set number of rows")
parser.add_argument("-cols", help="set number of cols")
parser.add_argument("-cell_size", help="set cell's size")
#Pour pouvoir accepter l'écriture de nombre décimaux dans le terminal, on doit utiliser des doubles tirets "--"
parser.add_argument("--afforestation", help="set simulation's afforestation")
parser.add_argument("--animation", help="set animation delay")

args = parser.parse_args()

if args.rows :
    rows = int(args.rows)
if args.cols :
    cols = int(args.cols)
if args.cell_size :
    cell_size = int(args.cell_size)
if args.animation :
    animation = float(args.animation)
if args.afforestation :
    afforestation = float(args.afforestation)

master = Tk()
canvas_height = (rows+2) * cell_size
canvas_width = (cols+2) * cell_size
canvas = Canvas(master, width = canvas_width, height = canvas_height)
canvas.pack()

v = StringVar() 

Radiobutton(master, text = "Règle 1", variable = v, value = "1").pack(fill = X)
Radiobutton(master, text = "Règle 2", variable = v, value = "2").pack(fill = X)
v.set("1")

foret = init_tableau()

if __name__ == "__main__":
    main()